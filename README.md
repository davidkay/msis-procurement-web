# msis-procurement-web

> MDE procurement submission in Nuxt/Vuetify

## Build Setup

``` bash
# install dependencies
npm i

# serve with hot reload at localhost:3000
nuxt -s
```

## API

https://msis-procurement-api.herokuapp.com

## Users

```json
{
  "email": "admin@mdek12.org",
  "password": "rock-ammonite",
},
{
  "email": "info@mdek12.org",
  "password": "humanoid-kentucky",
}
```
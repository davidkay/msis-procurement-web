const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: 'rebeccapurple',
    height: '5px'
  },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl'
  ],

  /*
  ** Plugins to load before mounting the App
  */
 plugins: [
  '@/plugins/vuetify',
  { src: '@/plugins/auth', ssr: false }
],

  // Nuxt Modules
    //  Axios for API
    //  Auth does...auth
    //  Toast does toast
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/toast'
  ],

  // TOASTYYYYYYYYYYYY
  toast: {
    position: 'bottom-center',
    duration: 1500
  },
  /*
  ** Axios module configuration
  */
  // config axios default API url
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    // baseURL:'http://localhost:3030',
    // browserBaseURL: 'http://localhost:3030',
    baseURL:'https://msis-procurement-api.herokuapp.com',
    browserBaseURL: 'https://msis-procurement-api.herokuapp.com'
  },
  router: {
    middleware: ['auth']
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {url: '/authentication', method: 'post', propertyName: 'accessToken' }, // 1. works
          logout: false,
          user: {url: '/authentication', method: 'post', propertyName: false }, //false,
        },
        // tokenRequired: true,
        // tokenType: 'Bearer'
      },
    },
    redirect: {
      // User will be redirected to this path if login is required.
      login: '/',
      // User will be redirected to this path if after logout, current route is protected.
      logout: '/',
      // User will be redirect to this path after login. (rewriteRedirects will rewrite this path)
      home: '/private/submissions'
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

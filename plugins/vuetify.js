// import Vue base
import Vue from 'vue'
import Vuetify from 'vuetify'

// import CSS
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'


// Navy #001f3f
// Blue #0074D9
// Aqua #7FDBFF
// TEAL #39CCCC
// OLIVE #3D9970
// GREEN #2ECC40
// LIME #01FF70
// YELLOW #FFDC00
// ORANGE #FF851B
// RED #FF4136
// MAROON #85144b
// FUCHSIA #F012BE
// PURPLE #B10DC9
// BLACK #111111
// GRAY #AAAAAA
// SILVER #DDDDDD

Vue.use(Vuetify, {
  theme: {
    primary: "#0074D9",     // blue
    secondary: "#39CCCC",   // teal
    accent: "#B10DC9",      // purple
    info: "#7FDBFF",        // aqua
    warning: "#FFDC00",     // yellow
    error: "#FF4136",       // red
    success: "#3D9970"      // olive
  }
})

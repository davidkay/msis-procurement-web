export default async function ({ app }) {
  console.log('auth check executed')
  if (!app.$auth.loggedIn) {
    return
  }

  const auth = app.$auth;

  const authStrategy = auth.strategy.name;
  if(authStrategy === 'facebook' || authStrategy === 'google'){
    console.log('inside first loop of auth')
    const token = auth.getToken(authStrategy).substr(7);
    const authStrategyConverted = authStrategy === 'facebook' ? 'fb' : 'google';
    const url = `/user/signup/${authStrategyConverted}?token=${token}`;

    try {
      const {data} = await app.$axios.$post(url, null);
      
      auth.setToken('local', "Bearer "+ data.accessToken);
      // auth.setUser(data.user[0])
      // store.dispatch('setUser', data.user[0]);
      setTimeout( async () => {
        console.log('first async after try')
        auth.setStrategy('local');
        setTimeout( async () => {
          await auth.fetchUser();
        })
      });
    } catch (e) {
      console.log(e);
    }
  } 
  // else if(authStrategy === 'local'){
  //   console.log('local strategy called in auth')
  //   
  //   const {data} = await app.$axios.$post('http://localhost:3030/authentication', null)
  //   
  //   auth.setToken('local', "Bearer " + accessToken)
  //   auth.setUser('local', user)
  // }
}

import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    strict: true,
    state: () => ({
      form: {
        entityName: '',
        contactFirstName: '',
        contactLastName: '',
        contactEmail: '',
        entityDuns: '',
        entityId: '',
        solNumber: '',
        solTitle: ''
      }
    }),
    mutations: {
      // update Company Name
      UPDATE_ENTITY_NAME: (state, payload) => {
        state.form.entityName = payload
      },
      // update first name
      UPDATE_FIRST_NAME: (state, payload) => {
        state.form.contactFirstName = payload
      },
      // update last name
      UPDATE_LAST_NAME: (state, payload) => {
        state.form.contactLastName = payload
      },
      // update email
      UPDATE_CONTACT_EMAIL: (state, payload) => {
        state.form.contactEmail = payload
      },
      // update duns
      UPDATE_ENTITY_DUNS: (state, payload) => {
        state.form.entityDuns = payload
      },
      // update duns
      UPDATE_ENTITY_ID: (state, payload) => {
        state.form.entityId = payload
      },
      // update solitation number
      UPDATE_SOL_NUMBER: (state, payload) => {
        state.form.solNumber = payload
      },
      // update solitation title
      UPDATE_SOL_TITLE: (state, payload) => {
        state.form.solTitle = payload
      }
    }
  })
}

export default createStore